var ymax=50,
    ymin=10,
    tmax=100,
    tmin=30,
    acl='active',
    wcl='win',
    finishC=$('#finish'),
    track=$('#track'),
    lines=track.find('li'),
    distance=500,
    gap=0,
    finish=distance - gap,
    elHeight=85,
    panel=$('#panel-message'),
    container=$('#container'),
    info=$('#info'),
    infoFull=$('#info-full'),
    bugs=$('.bug'),
    go=$('.b-go'),
    init=$('.b-init'),
    leaderBug=0,
    leader=0,
    showStartIntro=false,
    rushQ=40,
    topQ=40,

    inited=false;
var obj={};
var objScript={};

function rand(max,min){
    return Math.floor(Math.random() * (max - min) + min);
}

function showInfo(id,params){
    var rc='rush-cell';
    var rfc='rush-fail-cell';
    var name='rush';
    if(id){
        var a=params,
            el=obj[id -1];
        // Rush
        var rush=a.duration * a.ctop > rushQ;
        if(rush){
            if(a.ctop<=topQ)name+='-fail';
            var n=el[name] +1;
            obj[id -1][name]=n;
            el[name+'-cell'].html(n);
        }
    }else{
        for(var n in obj){
            obj[n][rc].html('...');
            obj[n][rfc].html('...');
        }
    }
}

function showLeader(id,now){
    var pc='pos-cell';
    for(var n in obj)obj[n][pc].removeClass(acl);
    leader=0;

    if(id){
        leader=now;
        var i=obj[id -1];
        i[pc].addClass(acl);
        if(leaderBug!=id){
            leaderBug=id;
            i["leader"]+=1;
            i[pc].html(i["leader"]);
        }
    }else for(var n in obj)obj[n][pc].html('...');
}

function showTime(id,now){
    var n='WINNER',
        tc='time-cell';
    if(id){
        if(now>.1)n=Number(distance - gap - now).toFixed(3);
        obj[id -1][tc].html(n);
    }else
        for(var n in obj)obj[n][tc].html('...');
}

function moveBug(id){
    var el=obj[id -1].el,
        i=objScript[id].shift(),
        timeout=i.timeout,
        duration=i.duration,
        cl='s-'+ duration,
        f=i.f;
    el.removeClass(wcl)
        .addClass(cl)
        .animate({
            top: i.top
        },{
        complete: function(){
            el.removeClass(cl).addClass(wcl);
            if(f){
                finishRace(id,el);
                showTime(id,-1);
            }else{
                showInfo(id,i);
                obj[id -1]["ls"]=setTimeout(function(){moveBug(id);},timeout);
            }
        },
        duration: duration *1000,
        step: function(now){
            if(now>leader)showLeader(id,now);
            showTime(id,now);
        }
    });
}

function createMove(id,top){
    var ctop=rand(ymax,ymin),
        timeout=rand(tmax,tmin),
        duration=rand(3,1),
        f=false;
    if(!top)top=0;
    top+=ctop;
    if(top>=finish){
        top=finish;
        f=true;
    }

    var a={ctop:ctop,top:top,timeout:timeout,duration:duration};
    if(f)a.f=true;

    createScript(id,a);
}

function createScript(id,a){
    if(!objScript[id])objScript[id]=[];
    objScript[id].push(a);

    if(!a.f)createMove(id,a.top);
}

function initList(){
    var counter=0;
    bugs.each(function(){
        var i=$(this),
            id=i.data('id');
        i.css('top',0);
        obj[counter]={
            'el': i,
            'top': 0,
            'leader': 0,
            'rush': 0,
            'rush-fail': 0,
            'rush-cell': container.find('.rush td[data-id="'+ id +'"]'),
            'rush-fail-cell': infoFull.find('.rush-fail td[data-id="'+ id +'"]'),
            'time-cell': container.find('.time td[data-id="'+ id +'"]'),
            'pos-cell': container.find('.pos td[data-id="'+ id +'"]'),
            'name-cell': container.find('.name td[data-id="'+ id +'"]')
        };
        counter++;
        createMove(id);
    });
}

function initRace(){
    initList();
    showInfo();
    showLeader();
    showTime();
    container.removeClass(wcl);
    lines.removeClass(wcl);
    panel.html('').slideUp();
    inited=true;
}

function goRace(){
    if(showStartIntro){
        go.fadeOut();
        panel.html('READY?').slideDown();
        var c=3;
        var ls=setInterval(function(){
            if(c){
                panel.html(c);
                c--;
            }else{
                clearInterval(ls);
                panel.html('').hide();
                startRace();
            }
        },1000);
    }else startRace();
}

function startRace(){
    for(var n in objScript)moveBug(n);
}

function finishRace(id,i){
    $('.bug').each(function(){
        $(this).stop().addClass(wcl);
    });
    for(var n in obj)clearTimeout(obj[n]["ls"]);
    panel.html('Winner #'+ id).slideDown();
    i.parent().addClass(wcl);
    go.fadeIn();
    container.addClass(wcl);
    inited=false;
}

init.click(function(){
    initRace();
});
go.click(function(){
    if(!inited)initRace();
    goRace();
});

track.css({height: distance+'px'});
finishC.css({top: distance + elHeight +'px'});
info.css({top: distance + 151 +'px'});